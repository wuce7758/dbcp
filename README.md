#dbcp
#主要功能是以tomcat7.0 DBCP为原型封装成为可移植的数据库连接工具包。可通用于JavaSE或JavaEE平台(适用于Java初学者试用)
#前几年学习中写的小东东，共享出来，希望能帮助更多的java初学者

#(一)说明：idemondbcp1.0,其主要功能是以tomcat7.0 DBCP为原型封装成为可移植的数据库连接工具包。可通用于JavaSE或JavaEE平台
#其中
#demo是用法演示
#doc是API文档
#lib是依赖第三方jar包
#src是源码
#dbconfig.properties是依赖的配置文件（需要用户根据实际情形进行配置）
#idemon-dbcp-1.0.jar是发布包



#(二)用法：用户只需将lib下的jar包、idemon-dbcp-1.0.jar以及相应数据库的JDBC驱动包一并添加到项目路径，并拷贝dbconfig.properties到项目classpath（或者src）下，按照实际情形进行相关配置,在需要用到数据库连接或者数据源的地方引入com.idemon.dbcp.DBUtil即可！



#(三)dbconfig.propertpeiz配置：

#==========下面字段都是可选，写明的值即是默认值==========#
#testWhileIdle=true
#testOnBorrow=true
#testOnReturn=false
#validationQuery=SELECT 1
#validationInterval=30000
#timeBetweenEvictionRunsMillis=30000
#maxActive=100
#minIdle=10
#maxWait=10000
#initialSize=10
#removeAbandonedTimeout=60
#removeAbandoned=true
#logAbandoned=true
#minEvictableIdleTimeMillis=30000
#jmxEnabled=true
#fairQueue=true
#jdbcInterceptors=org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer

#servername=localhost 可以是服务器名称、服务器IP地址（可选，默认“localhost”）
#serverport=3306 数据库服务器端口号（可选，Oracle默认1521，MySQL默认3306，SqlServer默认1433）


#==========下面字段都是必选，写明的值即是默认值==========#
#databaseType可以是Oracle9i、Oracle10g、Oracle11g、MySQL、SqlServer2000，SqlServer2005，SqlServer2008。
#databaseType=MySQL 
#databasename：数据库名称（必填，无默认值）
#username=root（必填，无默认值）
#password=1（必填，无默认值）