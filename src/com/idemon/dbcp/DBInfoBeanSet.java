package com.idemon.dbcp;

import java.util.Hashtable;
import java.util.Map;
/**
 * 该类用于封装多个DBInfoBean对象，
 * 也就是用于封装有关多个常用数据库的DBInfoBean信息
 * @author idemon
 *
 */
public class DBInfoBeanSet {

	private Map<String, DBInfoBean> dbinfos = new Hashtable<String, DBInfoBean>();
	
	public Map<String, DBInfoBean> getDbinfos() {
		return dbinfos;
	}
	
	public void addDbinfo(DBInfoBean dbinfo){
		dbinfos.put(dbinfo.getName(), dbinfo);
	}
}
