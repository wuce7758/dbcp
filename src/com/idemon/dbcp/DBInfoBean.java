package com.idemon.dbcp;
/**
 * 该类对应于默认配置文件dbinfo.xml，
 * 用于封装数据库的driverClassName和URL信息
 * @author idemon
 *
 */
public class DBInfoBean {

	/**
	 * 数据库类型名称
	 */
	private String name;
	/**
	 * 数据库驱动类名称
	 */
	private String driverClassName;
	/**
	 * 数据库连接串
	 */
	private String url;
	/**
	 * 数据库默认端口号
	 */
	private String port;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * 根据用户提供的数据库配置信息来完善DriverClass和URL
	 * @param serverName	数据库服务器名称或者地址
	 * @param serverPort	数据库服务器端口号
	 * @param databaseName	数据库名称
	 */
	public void fillUrl(String serverName,String serverPort,String databaseName){
		this.setUrl(this.url.replace("[servername]", serverName)
		.replace("[serverport]", serverPort).replace("[databasename]", databaseName));
	}
}
