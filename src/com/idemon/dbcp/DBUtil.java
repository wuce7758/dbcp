package com.idemon.dbcp;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
/**
 * 该类为用户提供获取数据源（DataSource）或者数据库连接（Connection）等对象的接口，
 * 并且对数据源（DataSource）或者数据库连接（Connection）进行管理
 * @author idemon
 *
 */
public class DBUtil {
	
	private static DataSource datasource;
	
	static{
		datasource = new DataSource();
        datasource.setPoolProperties(DBPoolPropFactory.getPoolProperties());
	}
	
	private DBUtil(){}
	/**
	 * 获取数据源（DataSource）
	 * @return 数据源
	 */
	public static DataSource getDataSource(){ 
		return datasource;
	}
	/**
	 * 获取数据库连接对象（Connection）
	 * @return 数据库连接对象
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		return datasource.getConnection();
	}
	/**
	 * 关闭数据库连接
	 * @param conn 数据库连接
	 */
	public static void close(Connection conn){
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 关闭数据源
	 * @param datasource 数据源
	 */
	public static void close(DataSource datasource){
		if(datasource != null){
			datasource.close();			
		}
	}
}
