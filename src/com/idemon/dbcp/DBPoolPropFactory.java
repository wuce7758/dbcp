package com.idemon.dbcp;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.digester3.Digester;
import org.apache.tomcat.jdbc.pool.PoolProperties;
/**
 * 该类用于读取context.xml、dbinfo.xml、dbconfig.properties（该文件是由用户配置）文件，
 * 初始化相关配置信息，
 * 并将配置信息封装到PoolProperties对象中，通过getPoolProperties()返回
 * @author idemon
 *
 */
public class DBPoolPropFactory {
	/**
	 * 用于封装所有配置信息
	 */
	private static PoolProperties poolprop;
	/**
	 * 用于封装常用数据库DriverClass和URL信息
	 */
	private static DBInfoBeanSet dbInfoBeanSet;
	/**
	 * 用于封装用户配置信息
	 */
	private static DBUserConfig dbUserConfig;
	
	static{
		dbUserConfig = new DBUserConfig();
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private DBPoolPropFactory(){}

	private static void init() throws Exception {
		
		readContextFile();
		readDbinfoFile();
		readDBUserConfigFile();
		completeDBInfo();		

	}
	/**
	 * 1)读取默认配置文件context.xml内容，并初始化PoolProperties对象
	 * @throws Exception
	 */
	private static void readContextFile() throws Exception{
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.addObjectCreate("Context/Resource", PoolProperties.class);
		digester.addSetProperties("Context/Resource");
		InputStream input = ReadConfigFileUtil.getResourceAsStream("context.xml");
		poolprop = (PoolProperties) digester.parse(input);
	}
	/**
	 * 2)读取默认配置文件dbinfo.xml内容，并初始化DBInfoBeanSet集合对象
	 * @throws Exception
	 */
	private static void readDbinfoFile() throws Exception{
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.addObjectCreate("dbinfos", DBInfoBeanSet.class);
		digester.addObjectCreate("dbinfos/dbinfo", DBInfoBean.class);
		digester.addBeanPropertySetter("dbinfos/dbinfo/name");
		digester.addBeanPropertySetter("dbinfos/dbinfo/driverClassName");
		digester.addBeanPropertySetter("dbinfos/dbinfo/url");
		digester.addBeanPropertySetter("dbinfos/dbinfo/port");
		digester.addSetNext("dbinfos/dbinfo", "addDbinfo","com.gym.dbcp.DBInfoBeanSet");
		InputStream input = ReadConfigFileUtil.getResourceAsStream("dbinfo.xml");
		dbInfoBeanSet = (DBInfoBeanSet)digester.parse(input);
	}
	/**
	 * 3)读取用户配置文件dbconfig.properties内容，并更新PoolProperties对象属性值
	 * @throws Exception
	 */
	private static void readDBUserConfigFile() throws Exception{
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = ReadConfigFileUtil.getResourceAsStream("dbconfig.properties");
		} catch (Exception e2) {
			String msg = "Can't find the 'dbconfig.properties' File,Please confirm the 'dbconfig.properties' File which @ classpath's root is created by User!";
			throw new Exception(msg,e2);
		}

		prop.load(input);

		Set<Entry<Object, Object>> props = prop.entrySet();
		Iterator<Entry<Object, Object>> it = props.iterator();

		while (it.hasNext()) {
			Entry<Object, Object> entry = it.next();
			String propertyName = entry.getKey().toString();
			//通过属性名称调用对应属性的setter方法来封装对应属性值
			StringBuilder builder = new StringBuilder("set");
			builder.append(propertyName.toUpperCase().substring(0, 1));
			builder.append(propertyName.substring(1));
			
			try {
				Method method = PoolProperties.class.getMethod(builder.toString(),String.class);
				method.invoke(poolprop, entry.getValue().toString());
			} catch (Exception e) {
				try {
					Method method = DBUserConfig.class.getMethod(builder.toString(),String.class);
					method.invoke(dbUserConfig, entry.getValue().toString());
				} catch (Exception e1) {
					throw new Exception("Can't find the correct property which is in 'dbconfig.properties',Please insure all the properties of 'dbconfig.properties' is correct!",e1);
				} 
			}
		}
	}
	/**
	 * 4)根据用户提供的配置信息完善PoolProperties对象中的DriverClass和URL数据库配置信息
	 * @throws Exception
	 */
	private static void completeDBInfo() throws Exception{
		if(dbUserConfig.getDatabaseType() != null && !dbUserConfig.getDatabaseType().isEmpty()){
			if(dbInfoBeanSet.getDbinfos().containsKey(dbUserConfig.getDatabaseType())){
				DBInfoBean dbinfo = dbInfoBeanSet.getDbinfos().get(dbUserConfig.getDatabaseType());
				if(null == dbUserConfig.getServerPort() || dbUserConfig.getServerPort().isEmpty()){
					dbUserConfig.setServerPort(dbinfo.getPort());
				}
				dbinfo.fillUrl(dbUserConfig.getServerName(), dbUserConfig.getServerPort(), dbUserConfig.getDatabaseName());
				poolprop.setUrl(dbinfo.getUrl());
				poolprop.setDriverClassName(dbinfo.getDriverClassName());
			}else{
				throw new Exception("The DatabaseType '"+dbUserConfig.getDatabaseType()+"' don't match the dbinfo.xml");
			}
		}else{
			throw new Exception("The property 'DatabaseType' must be explicit!");
		}
	}
	/**
	 * 获取数据库连接池相关属性对象
	 * @return PoolProperties
	 */
	static PoolProperties getPoolProperties(){
		return poolprop;
	}
	
}
