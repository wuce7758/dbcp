package com.idemon.dbcp;
/**
 * 该类对应于dbconfig.properties文件，
 * 用于封装dbconfig.properties（该文件由用户配置）中的配置信息
 * @author idemon
 *
 */
public class DBUserConfig {

	/**
	 * 所使用的数据库的类型
	 * databasetype's value may be 
	 * one of "Oracle9i,Oracle10g,Oracle11g,MySQL,SqlServer2000,SqlServer2005,SqlServer2008";
	 */
	private String databaseType;
	/**
	 * 数据库服务器名称或者ip,默认localhost
	 */
	private String serverName = "localhost";
	/**
	 * 数据库服务器端口号
	 */
	private String serverPort;
	/**
	 * 数据库[服务]名
	 */
	private String databaseName;
	
	public String getDatabaseType() {
		return databaseType;
	}
	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getServerPort() {
		return serverPort;
	}
	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
}
