package com.idemon.dbcp;

import java.io.InputStream;
/**
 * 
 * 该类用于读取相关配置文件
 * （一般是class类路径下的配置文件）
 * @author idemon
 *
 */
public class ReadConfigFileUtil {

	/**
	 * 打开并获取对应于给定名称的配置文件的输入流
	 * @param configFileName 配置文件名称（不带“/”）
	 * @return
	 * @throws Exception
	 */
	public static InputStream getResourceAsStream(String configFileName) throws Exception{
		InputStream input = null;
		String configFile = "/" + configFileName;
		if (input == null) {
			input = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);
		}
		if (input == null) {
			//尝试读取class文件所在的根目录下的配置文件
			input = DBPoolPropFactory.class.getResourceAsStream(configFile);
		}
		if(input == null){
			throw new Exception("Can't find the '"+configFileName+"' File,Please confirm the Config File Path!");
		}
		return input;
	}
}
