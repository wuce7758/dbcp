import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.idemon.dbcp.DBUtil;

public class Demo {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select * from dept");
		while(rs.next()){
			System.out.print(rs.getInt(1) + "\t");
			System.out.print(rs.getString(2) + "\t");
			System.out.print(rs.getString(3) + "\t");
			System.out.println();
		}
		DBUtil.close(conn);
	}

}